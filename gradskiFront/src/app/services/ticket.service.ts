import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of} from 'rxjs';
import { Config } from 'src/config';
import {TicketType} from '../model/ticketType';
import { Router } from '@angular/router'
import {NotificationService} from '../services/notification.service';

@Injectable({
  providedIn: 'root'
})
export class TicketService {
  httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',

  })
  };
  constructor(private http: HttpClient, private router: Router, private notificationService : NotificationService) { }
  private ticketUrl =  `${Config.apiUrl}ticket`;
  buyTicket(ticketType){

    this.http.post<any>(this.ticketUrl, ticketType, this.httpOptions)
    .subscribe(ticket => {this.router.navigate(["/myTickets"]); this.notificationService.success("Uspešno ste kupili " + ticketType.name + " kartu.")},
    
     );
  }
}
