package com.gsp.ns.gradskiPrevoz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
@EnableScheduling

public class GradskiPrevozApplication {

	public static void main(String[] args) {
		SpringApplication.run(GradskiPrevozApplication.class, args);
	}
}
